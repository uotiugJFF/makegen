/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_left_wins.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:10:12 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:04:38 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	new_win(t_env *e, t_win *win, int pos)
{
	int		p;
	int		clr;
	int		clr2;
	int		height;
	t_req	*req;
	t_file	*file;
	int		i;
	int		len;

	clr = 2;
	clr2 = 9;
	height = 3;
	if (win->type == VAR)
		clr = 4;
	else if (win->type == PHONY)
		clr = 8;
	else if (win->type == RULE || win->type == OBJECT)
	{
		height = 5 + get_lb_nb(win->value);
		clr = 5;
		clr2 = 4;
	}
	p = 85;
	if (COLS < 150)
		p = 0;
	win->pos = pos;
	win->win = newwin(height, COLS - p, e->left_win_pos, 0);
	refresh();
	if (e->win_sel == pos)
		wattron(win->win, COLOR_PAIR(clr));
	box(win->win, 0, 0);
	if (e->win_sel == pos)
		wattroff(win->win, COLOR_PAIR(clr));
	wattron(win->win, COLOR_PAIR(clr));
	if (win->type == OBJECT)
		mvwprintw(win->win, 0, 2, " $(%s)%%.o ", win->o_left->name);
	else
		mvwprintw(win->win, 0, 2, " %s ", win->name);
	wattroff(win->win, COLOR_PAIR(clr));
	if (win->files)
	{
		file = win->files;
		win->sub = newwin(count_files(win), COLS - p - 3, e->left_win_pos + 1, 2);
		wattron(win->win, COLOR_PAIR(clr2));
		i = 1;
		len = 0;
		wmove(win->win, 1, 2);
		while (file && i > 0)
		{
			if (file->selected)
			{
				if ((int)(len + 1 + strlen(file->name)) > COLS - p - 7)
				{
					wprintw(win->win, "...");
					i = -1;
				}
				else
				{
					wprintw(win->win, "%s ", file->name);
					i++;
					len += strlen(file->name) + 1;
				}
			}
			file = file->next;
		}
		wattroff(win->win, COLOR_PAIR(clr2));
	}
	if (win->type == VAR)
	{
		wattron(win->win, COLOR_PAIR(clr2));
		mvwprintw(win->win, 1, 2, "%s", win->value);
		wattroff(win->win, COLOR_PAIR(clr2));
	}
	else if (win->type == PHONY)
	{
		wmove(win->win, 1, 2);
		if (win->req)
		{
			req = win->req;
			while (req)
			{
				wattron(win->win, COLOR_PAIR(clr2));
				wprintw(win->win, "%s ", req->win->name);
				wattroff(win->win, COLOR_PAIR(clr2));
				req = req->next;
			}
		}
	}
	else if (win->type == RULE)
	{
		wmove(win->win, 1, 2);
		if (win->req)
		{
			req = win->req;
			while (req)
			{
				wattron(win->win, COLOR_PAIR(clr2));
				if (req->win->type == VAR)	
					wprintw(win->win, "$(%s) ", req->win->name);
				else
					wprintw(win->win, "%s ", req->win->name);
				wattroff(win->win, COLOR_PAIR(clr2));
				req = req->next;
			}
		}
		if (e->win_sel == pos)
			wattron(win->win, COLOR_PAIR(clr));
		mvwhline(win->win, 2, 1, ACS_HLINE, COLS - p - 2);
		if (e->win_sel == pos)
			wattroff(win->win, COLOR_PAIR(clr));
		win->sub = newwin(get_lb_nb(win->value) + 1, COLS - p - 3, e->left_win_pos + 3, 2);
		wprintw(win->sub, "%s", win->value);
	}
	else if (win->type == OBJECT)
	{
		wmove(win->win, 1, 2);
		wattron(win->win, COLOR_PAIR(clr2));
		wprintw(win->win, "$(%s)%%.c", win->o_right->name);
		wattroff(win->win, COLOR_PAIR(clr2));
		if (e->win_sel == pos)
			wattron(win->win, COLOR_PAIR(clr));
		mvwhline(win->win, 2, 1, ACS_HLINE, COLS - p - 2);
		if (e->win_sel == pos)
			wattroff(win->win, COLOR_PAIR(clr));
		win->sub = newwin(get_lb_nb(win->value) + 1, COLS - p - 3, e->left_win_pos + 3, 2);
		wprintw(win->sub, "%s", win->value);
	}
	if (e->win_sel == pos)
	{
		if (win->type != RULE)
			wmove(win->win, 1, 2 + strlen(win->value));
	}
	e->left_win_pos += height;
	wrefresh(win->win);
	if (win->type == RULE || win->type == OBJECT)
		wrefresh(win->sub);
	else
		wmove(win->win, 1, 2 + strlen(win->value) - e->cursor_pos);
}
void	add_wins(t_env *e)
{
	t_win	*win;
	int		i;
	
	clear();
	e->left_win_pos = 0;
	preview_win(e);
	status_win(e);
	win = *e->windows;
	i = 0;
	while (win)
	{
		new_win(e, win, i);
		win = win->next;
		i++;
	}
	focus_sel_win(e);
}

