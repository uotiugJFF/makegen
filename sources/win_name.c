/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_name.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:40:57 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/29 18:23:38 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"
/*
void	cflags_win(t_env *e)
{
	if (e->next_sel == CFLAGS)
		wmove(e->cflags, 1, 2 + strlen(e->s_cflags));
	e->cflags = newwin(3, COLS - 100, 9, 5);
	refresh();
	if(e->next_sel == CFLAGS)
		wattron(e->cflags, COLOR_PAIR(2));
	box(e->cflags, 0, 0);
	if (e->next_sel == CFLAGS)
		wattroff(e->cflags, COLOR_PAIR(2));
	mvwprintw(e->cflags, 0, 2, " CFLAGS ");
	mvwprintw(e->cflags, 1, 2, "%s", e->s_cflags);
	wrefresh(e->cflags);
}

void	cc_win(t_env *e)
{
	if (e->next_sel == CC)
		wmove(e->cc, 1, 2 + strlen(e->s_cc));
	e->cc = newwin(3, COLS - 100, 6, 5);
	refresh();
	if(e->next_sel == CC)
		wattron(e->cc, COLOR_PAIR(2));
	box(e->cc, 0, 0);
	if (e->next_sel == CC)
		wattroff(e->cc, COLOR_PAIR(2));
	mvwprintw(e->cc, 0, 2, " CC ");
	mvwprintw(e->cc, 1, 2, "%s", e->s_cc);
	wrefresh(e->cc);
}

void	name_win(t_env *e)
{
	if (e->next_sel == NAME)
		wmove(e->name, 1, 2 + strlen(e->s_name));
	e->name = newwin(3, COLS - 100, 3, 5);
	refresh();
	if (e->next_sel == NAME)
		wattron(e->name, COLOR_PAIR(2));
	box(e->name, 0, 0);
	if (e->next_sel == NAME)
		wattroff(e->name, COLOR_PAIR(2));
	mvwprintw(e->name, 0, 2, " NAME ");
	mvwprintw(e->name, 1, 2, "%s", e->s_name);
	wrefresh(e->name);
}
*/
