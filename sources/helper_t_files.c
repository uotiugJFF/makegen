/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_t_files.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/01 09:11:27 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 12:03:13 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

int	all_files_selected(t_win *win)
{
	t_file	*tmp;
	int		sel;

	sel = 1;
	tmp = win->files;
	while (tmp && sel)
	{
		sel = tmp->selected;
		tmp = tmp->next;
	}
	return (sel);
}

int	count_selected_files(t_win *win)
{
	t_file	*tmp;
	int		i;

	i = 0;
	tmp = win->files;
	while (tmp)
	{
		if (tmp-> selected)
			i++;
		tmp = tmp->next;
	}
	return (i);
}

void	toggle_file(t_env *e)
{
	t_file	*file;
	t_win	*win;
	int		i;
	int		set;

	e->win_sel = e->prev_win_sel;
	win = get_win_sel(e);
	e->win_sel = -1;
	file = win->files;
	if (e->list_sel == 0)
	{
		if (all_files_selected(win))
			set = 0;
		else
			set = 1;
		while (file)
		{
			file->selected = set;
			file = file->next;
		}
		return ;
	}
	i = 1;
	while (file)
	{
		if (i == e->list_sel)
		{
			if (file->selected)
				file->selected = 0;
			else
				file->selected = 1;
		}
		i++;
		file = file->next;
	}
}

t_file	*last_file(t_file *list)
{
	if (!list)
		return (NULL);
	while (list->next)
		list = list->next;
	return(list);
		return (NULL);
}

void	add_file(t_win *win, t_file *file)
{
	t_file	*tmp;
	t_file	*prev;

	if (!win || !file)
		return ;
	if (!win->files)
		win->files = file;
	else
	{
		tmp = win->files;
		if (strcmp(tmp->name, file->name) > 0)
		{
			file->next = win->files;
			win->files = file;
			return ;
		}
		prev = tmp;
		tmp = tmp->next;
		while (tmp)
		{
			if (strcmp(tmp->name, file->name) > 0)
			{
				file->next = tmp;
				prev->next = file;
				return ;
			}
			prev = tmp;
			tmp = tmp->next;
		}
	}
}

void	free_files(t_win *win)
{
	t_file	*file;
	t_file	*tmp;

	file = win->files;
	while (file)
	{
		tmp = file;
		file = file->next;
		free(tmp->name);
		free(tmp);
	}
	win->files = NULL;
}

t_file	*new_t_file(char *name)
{
	t_file	*new;

	new = malloc(sizeof(t_file));
	if (!new)
		perr("MALLOC ERROR");
	new->name = name;
	new->selected = 0;
	new->next = NULL;
	return (new);
}
