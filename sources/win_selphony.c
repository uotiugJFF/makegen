/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_selphony.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:36:16 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/30 15:41:54 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	selphony_win(t_env *e)
{
	int		cols;
	int		lines;
	t_win	*win;
	int		i;
	int		j;
	t_win	*prev_sel_win;
	t_req	*tmp;

	cols = v_in(COLS - 30, 30, 70);
	lines = v_in(LINES - 30, 10, 20);
	e->selphony = newwin(lines, cols,
			(LINES - lines) / 2, (COLS - cols)  / 2);
	e->selphony_c = newwin(lines - 2, cols - 2,
			(LINES - lines) / 2 + 1, (COLS - cols)  / 2 + 1);
	refresh();
	wattron(e->selphony, COLOR_PAIR(2));
	box(e->selphony, 0, 0);
	wattroff(e->selphony, COLOR_PAIR(2));
	mvwprintw(e->selphony, 0, 2, " SELECT RIQUIRED ");
	wrefresh(e->selphony);
	prev_sel_win = get_prev_win_sel(e);
	
	wattron(e->selphony_c, COLOR_PAIR(5) | A_BOLD);
	tmp = get_prev_win_sel(e)->req;
	wmove(e->selphony, 0, 2);
	while (tmp)
	{
		wprintw(e->selphony_c, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
	wattroff(e->selphony_c, COLOR_PAIR(5) | A_BOLD);
	
	win = *e->windows;
	i = 0;
	while (win)
	{
		if (win->type == RULE)
		{
			if (i == e->list_sel)
				wattron(e->selphony_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			if (is_required(prev_sel_win, win->name))
				mvwprintw(e->selphony_c, i + 1, 1, "[*] ");
			else
				mvwprintw(e->selphony_c, i + 1, 1, "[ ] ");
			wprintw(e->selphony_c, "%s", win->name);
			j = strlen(win->name);
			while (++j < cols - 7)
				wprintw(e->selphony_c, " ");
			if (i == e->list_sel)
				wattroff(e->selphony_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			i++;
		}
		win = win->next;
	}
	wrefresh(e->selphony_c);
}
