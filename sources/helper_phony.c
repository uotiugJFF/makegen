/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_phony.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:59:50 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/30 16:02:24 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

int	count_phony(t_env *e)
{
	int	nb;
	t_win	*win;

	nb = 0;
	win = *e->windows;
	while (win)
	{
		if (win->type == RULE)
			nb++;
		win = win->next;
	}
	return (nb);
}

void	toggle_phony(t_env *e)
{
	t_win	*win;
	t_req	*req;
	t_win	*sel;
	int		i;

	win = *e->windows;
	sel = NULL;
	i = 0;
	while (win)
	{
		if (win->type == RULE)
		{
			if (i == e->list_sel)
				sel = win;
			i++;
		}
		win = win->next;
	}
	win = get_prev_win_sel(e);
	req = win->req;
	while (req)
	{
		if(req->win == sel)
		{
			del_req(win, sel->name);
			return ;
		}
		req = req->next;
	}
	add_req_back(win->name, sel->name, e);
}
