/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_selfiles.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/12/01 11:49:50 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 12:04:19 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	selfiles_win(t_env *e)
{
	int		cols;
	int		lines;
	int		i;
	int		j;
	t_win	*win;
	t_file	*tmp;

	e->win_sel = e->prev_win_sel;
	win = get_win_sel(e);	
	e->win_sel = -1;
	cols = v_in(COLS - 30, 30, 70);
	lines = count_files(win) + 3;
	e->selfiles = newwin(lines, cols,
			(LINES - lines) / 2, (COLS - cols) / 2);
	e->selfiles_c = newwin(lines - 2, cols - 2,
			(LINES - lines) / 2 + 1, (COLS - cols) / 2 + 1);
	refresh();
	wattron(e->selfiles, COLOR_PAIR(2));
	box(e->selfiles, 0, 0);
	wattroff(e->selfiles, COLOR_PAIR(2));
	mvwprintw(e->selfiles, 0, 2, " SELECT FILES ");
	wrefresh(e->selfiles);
	tmp = win->files;
	i = 0;
	while (tmp)
	{
		if (i == e->list_sel)
			wattron(e->selfiles_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
		if (i == 0)
		{
			if (all_files_selected(win))
				mvwprintw(e->selfiles_c, i, 1, "[*] all");
			else
				mvwprintw(e->selfiles_c, i, 1, "[ ] all");
			j = 7;
		}
		else
		{
			if (tmp->selected)
				mvwprintw(e->selfiles_c, i, 1, "[*] %s", tmp->name);
			else
				mvwprintw(e->selfiles_c, i, 1, "[ ] %s", tmp->name);
			j = strlen(tmp->name) + 4;
			tmp = tmp->next;
		}
		while (++j < cols - 3)
			wprintw(e->selfiles_c, " ");
		if (i == e->list_sel)
			wattroff(e->selfiles_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
		wprintw(e->selfiles_c, "\n");
		i++;
	}
	wrefresh(e->selfiles_c);
}

