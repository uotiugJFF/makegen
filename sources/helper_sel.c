/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_sel.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:07:17 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/30 15:07:37 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	set_prev_sel(t_env *e)
{
	if (e->win_sel == 0)
		e->win_sel = count_win(e);
	e->win_sel--;
}

void	set_next_sel(t_env *e)
{
	e->win_sel = (e->win_sel + 1) % count_win(e);
}
