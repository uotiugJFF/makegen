/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/27 17:22:51 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:51:08 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

int main()
{
	t_env	e;
	t_win	*win;
	int 	key;

	win = NULL;
	e.windows = &win;
	init_vars(&e);
	init_scr(&e);
	while ((key = getch()) != KEY_F(1)) {
	//	wprintw(stdscr, "Keycode: %d\n", key);
	//	wrefresh(stdscr);
		if (key == KEY_RESIZE)
			resize_update(&e);
		if (e.mode == NORMAL)
			normal_mode(&e, key);
		else if (e.mode == INSERT)
			insert_mode(&e, key);
		else if (e.mode == SEARCH || e.mode == SEARCHF || e.mode == SEARCHF2)
			search_mode(&e, key);
		else if (e.mode == SELECT)
			select_mode(&e, key);
		else if (e.mode == SPHONY)
			phony_mode(&e, key);
		else if (e.mode == INPUT)
			input_mode(&e, key);
		else if (e.mode == OBJECTS)
			object_mode(&e, key);
	}

	endwin();
	return 0;
}
