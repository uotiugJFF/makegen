/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 16:19:22 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 17:40:02 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	save_object(t_env *e)
{
	t_win	*win;
	char	*str;

	str = malloc(sizeof(char)
			* strlen("mkdir -p $()\n$(CC) $(CFLAGS) -c -o $@ $<")
			+ strlen(e->o_left->name) + 1);
	if (!str)
		return ;
	str[0] = 0;
	strcat(str, "mkdir -p $(");
	strcat(str, e->o_left->name);
	strcat(str, ")\n$(CC) $(CFLAGS) -c -o $@ $<");

	win = new_t_win(strdup(""), str, OBJECT, 1);
	win->o_left = e->o_left;
	win->o_right = e->o_right;
	add_win_after(e, win);
	e->mode = NORMAL;
	clear();
	e->win_sel = e->prev_win_sel + 1;
	add_wins(e);
}

int	count_object(t_env *e)
{
	t_win	*win;
	int		i;

	win = *e->windows;
	i = 0;
	while (win)
	{
		if (win->type == VAR)
			i++;
		win = win->next;
	}
	return (i);
}

t_win	*get_obj_win(t_env *e)
{
	t_win	*win;
	int		i;

	win = *e->windows;
	i = 0;
	while (win)
	{
		if (win->type == VAR)
		{
			if (i == e->list_sel)
				return (win);
			i++;
		}
		win = win->next;
	}
	return (NULL);
}

void	set_object(t_env *e)
{
	if (e->col == 0)
		e->o_left = get_obj_win(e);
	if (e->col == 1)
		e->o_right = get_obj_win(e);
}

void	close_object(t_env *e)
{
	delwin(e->selobject);
	e->win_sel = e->prev_win_sel;
	clear();
	e->mode = NORMAL;
	e->list_sel = 0;
	add_wins(e);
}

void	object_win(t_env *e)
{
	int		cols;
	int		lines;
	t_win	*win;
	int		i;
	int		j;

	cols = v_in(COLS - 30, 30, 70);
	lines = v_in(LINES - 30, 10, 20);
	e->selobject = newwin(lines, cols,
			(LINES - lines) / 2, (COLS - cols)  / 2);
	e->selobject_c = newwin(lines - 2, cols - 2,
			(LINES - lines) / 2 + 1, (COLS - cols)  / 2 + 1);
	refresh();
	wattron(e->selobject, COLOR_PAIR(2));
	box(e->selobject, 0, 0);
	wattroff(e->selobject, COLOR_PAIR(2));
	mvwprintw(e->selobject, 0, 2, " SELECT O / C ");
	wrefresh(e->selobject);
	
	win = *e->windows;
	i = 0;
	while (win)
	{
		if (win->type == VAR)
		{
			if (e->o_left == NULL)
				e->o_left = win;
			if (e->o_right == NULL)
				e->o_right = win;
			if (i == e->list_sel && e->col == 0)
				wattron(e->selobject_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			if (win == e->o_left)
				mvwprintw(e->selobject_c, i, 1, "[*] ");
			else
				mvwprintw(e->selobject_c, i, 1, "[ ] ");
			wprintw(e->selobject_c, "%s", win->name);
			j = strlen(win->name);
			while (++j < (cols - 2) / 2 - 5)
				wprintw(e->selobject_c, " ");
			if (i == e->list_sel)
				wattroff(e->selobject_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
				

			if (i == e->list_sel && e->col == 1)
				wattron(e->selobject_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));


			if (win == e->o_right)
				mvwprintw(e->selobject_c, i, (cols - 2) / 2 + 2, "[*] ");
			else
				mvwprintw(e->selobject_c, i, (cols - 2) / 2 + 2, "[ ] ");
			wprintw(e->selobject_c, "%s", win->name);
			j = strlen(win->name);
			while (++j < (cols - 2) / 2 - 6)
				wprintw(e->selobject_c, " ");
			if (i == e->list_sel)
				wattroff(e->selobject_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			i++;
		}
		win = win->next;
	}
	mvwvline(e->selobject_c, 0, (cols - 2) / 2, ACS_VLINE, lines - 2);
	wrefresh(e->selobject_c);
}

void	add_object(t_env *e)
{
	add_wins(e);
	e->mode = OBJECTS;
	e->list_sel = 0;
	object_win(e);
}

void	add_rule(t_env *e)
{
	add_wins(e);
	e->mode = INPUT;
	input_win(e);
}

void	add_var(t_env *e)
{
	add_wins(e);
	e->mode = INPUT;
	input_win(e);
}

void	add_phony(t_env *e)
{
	t_win	*tmp;

	tmp = *e->windows;
	while (tmp)
	{
		if (tmp->type == PHONY)
			return ;
		tmp = tmp->next;
	}
	e->mode = NORMAL;
	add_win_after(e, new_t_win(strdup(".PHONY"),
				strdup(""), PHONY, 1));
	e->win_sel++;
	e->prev_key = -1;
	clear();
	add_wins(e);
}

void	input_win(t_env *e)
{
	int				cols;

	curs_set(1);
	cols = v_in(COLS - 30, 30, 70);
	e->input = newwin(3, cols,
			(LINES - 3) / 2, (COLS - cols) / 2);
	e->input_c = newwin(1, cols - 4,
			(LINES - 3) / 2 + 1, (COLS - cols) / 2 + 2);
	refresh();
	wattron(e->input, COLOR_PAIR(2));
	box(e->input, 0, 0);
	wattroff(e->input, COLOR_PAIR(2));
	mvwprintw(e->input, 0, 2, " ENTER NAME ");
	wrefresh(e->input);
	wattron(e->input_c, COLOR_PAIR(5) | A_BOLD);
	wprintw(e->input_c, "%s", e->input_str);
	wattroff(e->input_c, COLOR_PAIR(5) | A_BOLD);
	wrefresh(e->input_c);
}
