/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_status.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:42:06 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 09:59:52 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	status_win(t_env *e)
{
	int i;

	e->status = newwin(1, COLS, LINES - 1, 0);
	refresh();
	i = -1;
	wattron(e->status, COLOR_PAIR(3) | A_REVERSE | A_BOLD);
	while (++i < COLS)
		mvwprintw(e->status, 0, i, " ");
	wattroff(e->status, COLOR_PAIR(1) | A_REVERSE | A_BOLD);

	if (e->mode == NORMAL)
	{
		wattron(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
		mvwprintw(e->status, 0, 0, " NORMAL ");
		wattroff(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
	}
	else if (e->mode == INSERT)
	{
		wattron(e->status, COLOR_PAIR(2) | A_REVERSE | A_BOLD);
		mvwprintw(e->status, 0, 0, " INSERT ");
		wattroff(e->status, COLOR_PAIR(2) | A_REVERSE | A_BOLD);
	}
	else if (e->mode == SEARCH || e->mode == SEARCHF || e->mode == SEARCHF2)
	{
		wattron(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
		mvwprintw(e->status, 0, 0, " SEARCH ");
		wattroff(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
	}
	else if (e->mode == SELECT || e->mode == SPHONY)
	{
		wattron(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
		mvwprintw(e->status, 0, 0, " SELECT ");
		wattroff(e->status, COLOR_PAIR(9) | A_REVERSE | A_BOLD);
	}
	wattron(e->status, COLOR_PAIR(1) | A_REVERSE | A_BOLD);
	mvwprintw(e->status, 0, COLS - 24, " MAKE GENERATOR ");
	wattroff(e->status, COLOR_PAIR(1) | A_REVERSE | A_BOLD);
	
	wattron(e->status, COLOR_PAIR(2) | A_REVERSE | A_BOLD);
	mvwprintw(e->status, 0, COLS - 8, " v0.0.1 ");
	wattroff(e->status, COLOR_PAIR(2) | A_REVERSE | A_BOLD);
	
	wrefresh(e->status);
}

