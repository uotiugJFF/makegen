/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_keys.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:47:45 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:26:34 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

int	is_tab(int key)
{
	if (key == KEY_STAB || key == 9)
		return (1);
	return (0);
}

int is_enter(int key)
{
	if (key == KEY_ENTER || key == 10)
		return (1);
	return (0);
}

void	normal_mode(t_env *e, int key)
{
	if ((is_tab(key) || is_enter(key) || key == KEY_DOWN))
	{
		set_next_sel(e);
		add_wins(e);
	}
	else if (key == KEY_UP)
	{
		set_prev_sel(e);
		add_wins(e);
	}
	else if (key == 'i')
	{
		e->mode = INSERT;
		e->cursor_pos = 0;
		add_wins(e);
	}
	else if (key == 'w')
	{
		save_makefile(e);
	}
	else if (key == 'd' && get_win_sel(e)->type == VAR)
	{
		if (e->prev_key == 'o')
		{
			e->prev_win_sel = e->win_sel;
			e->win_sel = -1;
			e->mode = SEARCH;
			add_wins(e);
			seldir_win(e);
		}
	}
	else if (key == 'f' && get_win_sel(e)->type == VAR)
	{
		if (e->prev_key == 'o')
		{
			e->prev_win_sel = e->win_sel;
			e->win_sel = -1;
			e->mode = SEARCHF;
			add_wins(e);
			seldir_win(e);
		}
	}
	else if (key == 's' && get_win_sel(e)->type == RULE)
	{
		e->prev_win_sel = e->win_sel;
		e->win_sel = -1;
		e->mode = SELECT;
		add_wins(e);
		selreq_win(e);
	}
	else if (key == 's' && get_win_sel(e)->type == PHONY)
	{
		e->prev_win_sel = e->win_sel;
		e->win_sel = -1;
		e->mode = SPHONY;
		add_wins(e);
		selphony_win(e);
	}
	else if (key == 'v')
	{
		if (e->prev_key == 'a')
		{
			e->add_type = VAR;
			e->prev_win_sel = e->win_sel;
			e->win_sel = -1;
			add_var(e);
		}
	}
	else if (key == 'r')
	{
		if (e->prev_key == 'a')
		{
			e->add_type = RULE;
			e->prev_win_sel = e->win_sel;
			e->win_sel = -1;
			add_rule(e);
		}
	}
	else if (key == 'p')
	{
		if (e->prev_key == 'a')
		{
			e->prev_win_sel = e->win_sel;
			add_phony(e);
		}
	}
	else if (key == 'o')
	{
		if (e->prev_key == 'a')
		{
			e->o_left = NULL;
			e->o_right = NULL;
			e->col = 0;
			e->prev_win_sel = e->win_sel;
			e->win_sel = -1;
			add_object(e);
		}
	}
	if (key == 'd')
	{
		if (e->prev_key == 'd')
		{
			e->prev_key = -1;
			del_t_win(e);
			clear();
			add_wins(e);
		}
		else
			e->prev_key = key;
	}
	else
		e->prev_key = key;
}

void	insert_mode(t_env *e, int key)
{
	if (key == KEY_LEFT)
		cursor_left(e);
	else if (key == KEY_RIGHT)
		cursor_right(e);
	else if (key == KEY_BACKSPACE || key == 263 || key == 127)
		do_backspace(e);
	else if (isprint(key) || (is_enter(key) && get_win_sel(e)->type == RULE))
		get_ch(e, key);
	else if (key == KEY_EXIT || key == 27)
	{
		e->mode = NORMAL;
		e->prev_key = -1;
	}
	else if (is_enter(key) && get_win_sel(e)->type == VAR)
	{
		e->mode = NORMAL;
		e->prev_key = -1;
		set_next_sel(e);
	}
	add_wins(e);
}

void	search_mode(t_env *e, int key)
{
	if (key == KEY_UP)
	{
		e->list_sel = max(e->list_sel - 1, 0);
		if (e->mode == SEARCHF2)
			selfiles_win(e);
		else
			seldir_win(e);
	}
	else if (key == KEY_DOWN)
	{
		if (e->mode == SEARCHF2)
		{
			e->win_sel = e->prev_win_sel;
			e->list_sel = min(e->list_sel + 1, count_files(get_win_sel(e)) - 1);
			e->win_sel = -1;
			selfiles_win(e);
		}
		else
		{
			e->list_sel = min(e->list_sel + 1, get_nb_files(e->dir) - 1);
			seldir_win(e);
		}
	}
	else if (is_enter(key))
	{
		if (e->mode == SEARCHF2)
			close_selfiles(e);
		else if (e->list_sel == 0 && e->mode == SEARCH)
			save_dir(e);
		else if (e->list_sel == 0 && e->mode == SEARCHF)
			save_files(e);
		else if (e->list_sel == 1)
			get_prevdir(e);
		else
			get_subdir(e);
	}
	else if (key == 32 && e->mode == SEARCHF2)
	{
		toggle_file(e);
		selfiles_win(e);
	}
	else if ((key == KEY_EXIT || key == 27) && e->mode == SEARCHF2)
		close_selfiles(e);
	else if (key == KEY_EXIT || key == 27)
		close_seldir(e);
}

void	select_mode(t_env *e, int key)
{
	if (key == KEY_UP)
	{
		e->list_sel = max(e->list_sel - 1, 0);
		selreq_win(e);
	}
	else if (key == KEY_DOWN)
	{
		e->list_sel = min(e->list_sel + 1, count_req(e) - 1);
		selreq_win(e);
	}
	else if (key == 32)
	{
		toggle_required(e);
		selreq_win(e);
	}
	else if (is_enter(key))
		close_selreq(e);
	else if (key == KEY_EXIT || key == 27)
		close_selreq(e);
}

void	object_mode(t_env *e, int key)
{
	if (key == KEY_UP)
	{
		e->list_sel = max(e->list_sel - 1, 0);
		object_win(e);
	}
	else if (key == KEY_DOWN)
	{
		e->list_sel = min(e->list_sel + 1, count_object(e) - 1);
		object_win(e);
	}
	else if (key == KEY_RIGHT)
	{
		e->col = 1;
		object_win(e);
	}
	else if (key == KEY_LEFT)
	{
		e->col = 0;
		object_win(e);
	}
	else if (key == 32)
	{
		set_object(e);
		object_win(e);
	}
	else if (is_enter(key))
	{
		save_object(e);
	}
	else if (key == KEY_EXIT || key == 27)
		close_object(e);
}

void	phony_mode(t_env *e, int key)
{
	if (key == KEY_UP)
	{
		e->list_sel = max(e->list_sel - 1, 0);
		selphony_win(e);
	}
	else if (key == KEY_DOWN)
	{
		e->list_sel = min(e->list_sel + 1, count_phony(e) - 1);
		selphony_win(e);
	}
	else if (key == 32)
	{
		toggle_phony(e);
		selphony_win(e);
	}
	else if (is_enter(key))
		close_selphony(e);
	else if (key == KEY_EXIT || key == 27)
		close_selphony(e);
}

void	input_mode(t_env *e, int key)
{
	if (key == KEY_BACKSPACE || key == 263 || key == 127)
	{
		do_backspace_input(e);
		input_win(e);
	}
	else if (isprint(key))
	{
		get_ch_input(e, key);
		input_win(e);
	}
	else if (is_enter(key))
		save_input(e);
	else if (key == KEY_EXIT || key == 27)
		close_input(e);
}
