/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_wins.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:45:20 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:07:37 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

t_win	*new_t_win(char *name, char *value, enum w_type type, int can_del)
{
	t_win	*new;

	new = malloc(sizeof(t_win));
	if (!new)
		perr("MALLOC ERRROR");
	new->name = name;
	new->value = value;
	new->prev = NULL;
	new->next = NULL;
	new->win = NULL;
	new->can_del = can_del;
	new->type = type;
	new->req = NULL;
	new->files = NULL;
	new->o_left = NULL;
	new->o_right = NULL;
	return (new);
}

void	del_req_if_needed(t_env *e, t_win *del_win)
{
	t_win	*win;
	t_req	*req;

	win = *e->windows;
	while (win)
	{
		req = win->req;
		while (req)
		{
			if (req->win == del_win)
				del_req(win, del_win->name);
			req = req->next;
		}
		win = win->next;
	}
}

void	del_t_win(t_env *e)
{
	t_win	*win;

	win = get_win_sel(e);
	if (win->can_del)
	{
		del_req_if_needed(e, win);
		free(win->name);
		free(win->value);
		if (!win->prev)
		{
			if (win->next)
				win->next->prev = NULL;
			*e->windows = win->next;
		}
		else if (win->next)
		{
			win->prev->next = win->next;
			win->next->prev = win->prev;
		}
		else
		{
			set_prev_sel(e);	
			win->prev->next = NULL;
		}
		free(win);
	}
}

t_win	*last_win(t_win *list)
{
	if (!list)
		return (NULL);
	while (list->next)
		list = list->next;
	return (list);
}

void	add_win_back(t_win **list, t_win *win)
{
	t_win	*last;

	if (!win)
		return ;
	if (list)
	{
		if (!*list)
			*list = win;
		else
		{
			last = last_win(*list);
			last->next = win;
			win->prev = last;
		}
	}
}

void	add_win_after(t_env *e, t_win *win)
{
	t_win	*tmp;
	t_win	**list;
	int		i;

	list = e->windows;
	if (!win)
		return ;
	if (list)
	{
		if (!*list)
			*list = win;
		else
		{
			i = 0;
			tmp = *list;
			while (tmp)
			{
				if (i == e->prev_win_sel)
					break;
				tmp = tmp->next;
				i++;
			}
			if (tmp->next)
				win->next = tmp->next;
			win->prev = tmp;
			tmp->next = win;
		}
	}
}

int	count_win(t_env *e)
{
	t_win	*tmp;
	int		nb;

	nb = 1;
	tmp = *e->windows;
	while (tmp->next)
	{
		tmp = tmp->next;
		nb++;
	}
	return (nb);
}

t_win	*get_prev_win_sel(t_env *e)
{
	t_win	*tmp;
	int		nb;

	nb = 0;
	tmp = *e->windows;
	while (tmp)
	{
		if (nb == e->prev_win_sel)
			return (tmp);
		tmp = tmp->next;
		nb++;
	}
	return (NULL);
}

t_win	*get_win_sel(t_env *e)
{
	t_win	*tmp;
	int		nb;

	nb = 0;
	tmp = *e->windows;
	while (tmp)
	{
		if (nb == e->win_sel)
			return (tmp);
		tmp = tmp->next;
		nb++;
	}
	return (NULL);
}

t_win *get_win_named(char *name, t_env *e)
{
	t_win	*tmp;

	tmp = *e->windows;
	while (tmp)
	{
		if (strcmp(name, tmp->name) == 0)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

int	get_lb_nb(char *str)
{
	int	i;
	int nb;

	i = -1;
	nb = 0;
	while (str[++i])
		if (str[i] == '\n')
			nb++;
	return (nb);
}

void	resize_update(t_env *e)
{
	endwin();
	clear();
	add_wins(e);
}

void	focus_sel_win(t_env *e)
{
	t_win	*win;

	curs_set(0);
	win = get_win_sel(e);
	if (win)
	{
		wrefresh(win->win);
		if (win->type == RULE || win->type == OBJECT)
			wrefresh(win->sub);
		if (e->mode == INSERT)
			curs_set(1);
	}
}
