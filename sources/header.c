/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:34:32 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/29 21:30:04 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

char	*header()
{
	char	*str;
	char	*sl = "# ";
	char	*el = " #\n";
	char	*elf = " #";
	char	*hl = "*";
	char	*l1 = "        :::      ::::::::";
	char	*l2 = "      :+:      :+:    :+:";
	char	*l3 = "    +:+ +:+         +:+  ";
	char	*l4 = "  +#+  +:+       +#+     ";
	char	*l5 = "+#+#+#+#+#+   +#+        ";
	char	*l6 = "     #+#    #+#          ";
	char	*l7 = "    ###   ########.fr    ";
	char	*uname = "gbrunet";
	char	*umail = "gbrunet@student.42.fr";
	char	*cdate;
	char	*udate;
	int		i;
	time_t now = time(NULL);
	struct tm *t = localtime(&now);

	str = malloc(sizeof(char) * 892);
	if (!str)
		return (NULL);
	str[0] = 0;
	
	cdate = malloc(sizeof(char) * 21);
	if (!cdate)
		return (NULL);
	cdate[0] = 0;
	
	udate = malloc(sizeof(char) * 21);
	if (!udate)
		return (NULL);
	udate[0] = 0;
	
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 76)
		strncat(str, hl, strlen(hl));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 76)
		strncat(str, " ", strlen(" "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 48)
		strncat(str, " ", strlen(" "));
	strncat(str, l1, strlen(l1));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	strncat(str, "   Makefile", strlen("   Makefile"));
	i = 0;
	while (i++ < 37)
		strncat(str, " ", strlen(" "));
	strncat(str, l2, strlen(l2));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 48)
		strncat(str, " ", strlen(" "));
	strncat(str, l3, strlen(l3));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	strncat(str, "   By: ", strlen("   By: "));
	strncat(str, uname, strlen(uname));
	strncat(str, " <", strlen(" <"));
	strncat(str, umail, strlen(umail));
	strncat(str, ">", strlen(">"));
	i = 0;
	while (i++ < 38 - (int)strlen(uname) - (int)strlen(umail))
		strncat(str, " ", strlen(" "));
	strncat(str, l4, strlen(l4));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 48)
		strncat(str, " ", strlen(" "));
	strncat(str, l5, strlen(l5));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	strncat(str, "   Created: ", strlen("   Created: "));
	strftime(cdate, 20, "%Y/%m/%d %H:%M:%S", t);
	strncat(str, cdate, strlen(cdate));
	strncat(str, " by ", strlen(" by "));
	strncat(str, uname, strlen(uname));
	i = 0;
	while (i++ < 32 - (int)strlen(cdate) - (int)strlen(uname))
		strncat(str, " ", strlen(" "));
	strncat(str, l6, strlen(l6));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	strncat(str, "   Updated: ", strlen("   Updated: "));
	strftime(udate, 20, "%Y/%m/%d %H:%M:%S", t);
	strncat(str, udate, strlen(udate));
	strncat(str, " by ", strlen(" by "));
	strncat(str, uname, strlen(uname));
	i = 0;
	while (i++ < 32 - (int)strlen(udate) - (int)strlen(uname))
		strncat(str, " ", strlen(" "));
	strncat(str, l7, strlen(l7));
	strncat(str, "   ", strlen("   "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 76)
		strncat(str, " ", strlen(" "));
	strncat(str, el, strlen(el));
	strncat(str, sl, strlen(sl));
	i = 0;
	while (i++ < 76)
		strncat(str, hl, strlen(hl));
	strncat(str, elf, strlen(elf));

	return (str);
}
