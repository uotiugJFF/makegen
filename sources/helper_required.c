/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_required.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:08:39 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/30 16:02:59 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

int	is_required(t_win *win, char *name)
{
	t_req	*req;

	req = win->req;
	while (req)
	{
		if (strcmp(req->win->name, name) == 0)
			return (1);
		req = req->next;
	}
	return (0);
}

t_req	*last_req(t_req *list)
{
	if (!list)
		return (NULL);
	while (list->next)
		list = list->next;
	return(list);
		return (NULL);
}

void	del_req(t_win *win, char *win_del)
{
	t_req	*req_del;

	req_del = win->req;
	if (!req_del)
		return ;
	while (req_del)
	{
		if (strcmp(req_del->win->name, win_del) == 0)
			break;
		req_del = req_del->next;
	}
	if (req_del->prev)
	{
		if (req_del->next)
		{
			req_del->prev->next = req_del->next;
			req_del->next->prev = req_del->prev;
		}
		else 
			req_del->prev->next = NULL;
	}
	else
	{
		if (req_del->next)
		{
			win->req = req_del->next;
			win->req->prev = NULL;
		}
		else
			win->req = NULL;
	}
	free(req_del);
}

void	add_req_back(char *win_str, char *req_win_str, t_env *e)
{
	t_win	*win;
	t_win	*win_req;
	t_req	*req;
	t_req	*last;

	win = get_win_named(win_str, e);
	win_req = get_win_named(req_win_str, e);
	if (!win || !win_req)
		return ;
	req = new_t_req(win_req);
	if (!req)
		return ;
	if (!win->req)
		win->req = req;
	else
	{
		last = last_req(win->req);
		last->next = req;
		req->prev = last;
	}
}

t_req	*new_t_req(t_win *win)
{
	t_req	*new;

	new = malloc(sizeof(t_req));
	if (!new)
		perr("MALLOC ERROR");
	new->win = win;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

int	count_req(t_env *e)
{
	int	nb;
	t_win	*win;

	nb = 0;
	win = *e->windows;
	while (win)
	{
		if ((win->type == RULE && win->name[0] != '$') || win->type == VAR)
			nb++;
		win = win->next;
	}
	return (nb);
}

void	toggle_required(t_env *e)
{
	t_win	*win;
	t_req	*req;
	t_win	*sel;
	int		i;

	win = *e->windows;
	sel = NULL;
	i = 0;
	while (win)
	{
		if ((win->type == RULE && win->name[0] != '$') || win->type == VAR)
		{
			if (i == e->list_sel)
				sel = win;
			i++;
		}
		win = win->next;
	}
	win = get_prev_win_sel(e);
	req = win->req;
	while (req)
	{
		if(req->win == sel)
		{
			del_req(win, sel->name);
			return ;
		}
		req = req->next;
	}
	add_req_back(win->name, sel->name, e);
}

