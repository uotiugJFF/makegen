/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_strings.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:52:20 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 14:21:53 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	cursor_left(t_env *e)
{
	t_win	*win;
	
	win = get_win_sel(e);
	e->cursor_pos = min(strlen(win->value), e->cursor_pos + 1);
}

void	cursor_right(t_env *e)
{
	e->cursor_pos = max(0, e->cursor_pos - 1);
}

void	remove_last_c(char *str, t_env *e)
{
	int	i;

	if (e->cursor_pos == 0)
	{
		if (strlen(str) > 0)
			str[strlen(str) - 1] = 0;
	}
	else if ((int)strlen(str) != e->cursor_pos)
	{
		i = -1;
		while (++i < (int)strlen(str))
		{
			if (i >= (int)strlen(str) - e->cursor_pos - 1)
				str[i] = str[i + 1];
		}
		str[i] = 0;
	}
}

void	do_backspace_input(t_env *e)
{
	remove_last_c(e->input_str, e);
}

void	get_ch_input(t_env *e, int key)
{
	e->input_str = update_str(e->input_str, key, e);
}

void	do_backspace(t_env *e)
{
	t_win	*win;

	win = get_win_sel(e);
	if (win)
		remove_last_c(win->value, e);
}

char	*update_str(char *str, int key, t_env *e)
{
	char	*dup;
	int		i;

	dup = strdup(str);
	free(str);
	str = malloc(sizeof(char) * (strlen(dup) + 2));
	if (e->cursor_pos == 0)
	{
		strncpy(str, dup, strlen(dup));
		str[strlen(dup)] = key;
		str[strlen(dup) + 1] = 0;
	}
	else
	{
		i = -1;
		while (++i < (int)strlen(dup) + 2)
		{
			if (i < (int)strlen(dup) - e->cursor_pos)
				str[i] = dup[i];
			else if (i == (int)strlen(dup) - e->cursor_pos)
			{
				str[i] = key;
			}
			else if (i > (int)strlen(dup) - e->cursor_pos)
				str[i] = dup[i - 1];
		}
		str[i] = 0;
	}
	free(dup);
	return (str);
}

void	get_ch(t_env *e, int key)
{
	t_win	*win;

	win = get_win_sel(e);
	if (win)
		win->value = update_str(win->value, key, e);
}

char	*str_upper(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		str[i] = toupper(str[i]);
	return (str);
}
