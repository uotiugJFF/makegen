/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_selreq.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/30 15:34:00 by gbrunet           #+#    #+#             */
/*   Updated: 2023/11/30 16:03:30 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	selreq_win(t_env *e)
{
	int		cols;
	int		lines;
	t_win	*win;
	int		i;
	int		j;
	t_win	*prev_sel_win;
	t_req	*tmp;

	cols = v_in(COLS - 30, 30, 70);
	lines = v_in(LINES - 30, 10, 20);
	e->selreq = newwin(lines, cols,
			(LINES - lines) / 2, (COLS - cols)  / 2);
	e->selreq_c = newwin(lines - 2, cols - 2,
			(LINES - lines) / 2 + 1, (COLS - cols)  / 2 + 1);
	refresh();
	wattron(e->selreq, COLOR_PAIR(2));
	box(e->selreq, 0, 0);
	wattroff(e->selreq, COLOR_PAIR(2));
	mvwprintw(e->selreq, 0, 2, " SELECT RIQUIRED ");
	wrefresh(e->selreq);
	prev_sel_win = get_prev_win_sel(e);
	
	wattron(e->selreq_c, COLOR_PAIR(5) | A_BOLD);
	tmp = get_prev_win_sel(e)->req;
	wmove(e->selreq, 0, 2);
	while (tmp)
	{
		if (tmp->win->type == VAR)
			wprintw(e->selreq_c, "$(%s) ", tmp->win->name);
		else
			wprintw(e->selreq_c, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
	wattroff(e->selreq_c, COLOR_PAIR(5) | A_BOLD);
	
	win = *e->windows;
	i = 0;
	while (win)
	{
		if ((win->type == RULE && win->name[0] != '$') || win->type == VAR)
		{
			if (i == e->list_sel)
				wattron(e->selreq_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			if (is_required(prev_sel_win, win->name))
				mvwprintw(e->selreq_c, i + 1, 1, "[*] ");
			else
				mvwprintw(e->selreq_c, i + 1, 1, "[ ] ");
			wprintw(e->selreq_c, "%s", win->name);
			j = strlen(win->name);
			while (++j < cols - 7)
				wprintw(e->selreq_c, " ");
			if (i == e->list_sel)
				wattroff(e->selreq_c, A_BOLD | A_REVERSE | COLOR_PAIR(2));
			i++;
		}
		win = win->next;
	}
	wrefresh(e->selreq_c);
}
