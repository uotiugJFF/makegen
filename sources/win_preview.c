/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_preview.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:38:55 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 17:43:37 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	pprint_var(t_env *e, t_win *win)
{
	int		nb;
	t_file	*tmp;
	int		len;

	wattron(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "\n\n%s ", win->name);
	wattroff(e->preview_c, COLOR_PAIR(4));
	wattron(e->preview_c, COLOR_PAIR(9));
	if (win->files)
	{
		nb = count_selected_files(win);
		tmp = win->files;
		wprintw(e->preview_c, "=");
		len = strlen(win->name) + 3;
		while (tmp)
		{
			if (tmp->selected)
			{
				nb--;
				if (len + strlen(tmp->name) + 3 > 80)
				{
					wprintw(e->preview_c, " \\\n    %s", tmp->name);
					len = strlen(tmp->name) + 4;
				}
				else
				{
					wprintw(e->preview_c, " %s", tmp->name);
					len += strlen(tmp->name) + 1;
				}
			}
			tmp = tmp->next;
		}
		
	}
	else
		wprintw(e->preview_c, "= %s", win->value);
	wattroff(e->preview_c, COLOR_PAIR(9));
}

void	pprint_phony(t_env *e, t_win *win)
{
	t_req	*tmp;

	wattron(e->preview_c, COLOR_PAIR(8));
	wprintw(e->preview_c, "\n\n%s : ", win->name);
	wattroff(e->preview_c, COLOR_PAIR(8));
	wattron(e->preview_c, COLOR_PAIR(9));
	tmp = win->req;
	while (tmp)
	{
		wprintw(e->preview_c, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
	wattroff(e->preview_c, COLOR_PAIR(9));
}

void	pprint_rule(t_env *e, t_win *win)
{
	t_req	*tmp;
	char	**lines;
	int		i;

	wattron(e->preview_c, COLOR_PAIR(5));
	wprintw(e->preview_c, "\n\n%s : ", win->name);
	wattroff(e->preview_c, COLOR_PAIR(5));
	wattron(e->preview_c, COLOR_PAIR(4));
	tmp = win->req;
	while (tmp)
	{
		if (tmp->win->type == VAR)
			wprintw(e->preview_c, "$(%s) ", tmp->win->name);
		else
			wprintw(e->preview_c, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
	wattroff(e->preview_c, COLOR_PAIR(4));
	lines = ft_split(win->value, '\n');
	if (!lines)
		return ;
	i = -1;
	while (lines[++i])
	{
		wprintw(e->preview_c, "\n    %s ", lines[i]);
		free(lines[i]);
	}
	free(lines);
}

void	pprint_object(t_env *e, t_win *win)
{
	t_req	*tmp;
	char	**lines;
	int		i;

	wattron(e->preview_c, COLOR_PAIR(5));
	wprintw(e->preview_c, "\n\n$(%s)%%.o : ", win->o_left->name);
	wattroff(e->preview_c, COLOR_PAIR(5));
	wattron(e->preview_c, COLOR_PAIR(4));
	tmp = win->req;
	wprintw(e->preview_c, "$(%s)%%.c", win->o_right->name);
	wattroff(e->preview_c, COLOR_PAIR(4));
	lines = ft_split(win->value, '\n');
	if (!lines)
		return ;
	i = -1;
	while (lines[++i])
	{
		wprintw(e->preview_c, "\n    %s ", lines[i]);
		free(lines[i]);
	}
	free(lines);
}

void	preview_print(t_env *e, t_win *win)
{
	int	clr1;
	int	clr2;

	clr1 = 9;
	clr2 = 9;
	if (win->type == VAR)
		pprint_var(e, win);
	else if (win->type == PHONY)
		pprint_phony(e, win);
	else if (win->type == RULE)
		pprint_rule(e, win);
	else if (win->type == OBJECT)
		pprint_object(e, win);
}

void	preview_win(t_env *e)
{
	t_win	*win;

	if (COLS < 150)
		return ;
	e->preview = newwin(LINES - 1, 84, 0, COLS - 84);
	e->preview_c = newwin(LINES - 3, 81, 1, COLS - 82);
	refresh();
	box(e->preview, 0, 0);
	mvwprintw(e->preview, 0, 2, " PREVIEW ");

	wattron(e->preview_c, COLOR_PAIR(3));
	wprintw(e->preview_c, "%s", header());
	wattroff(e->preview_c, COLOR_PAIR(3));

	win = *e->windows;

	while (win)
	{
		preview_print(e, win);
		win = win->next;
	}

/*	
	wattron(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "NAME ");
	wattroff(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "= %s", e->s_name);
	
	wattron(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "\n\nCC ");
	wattroff(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "= %s", e->s_cc);
	
	wattron(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "\n\nCFLAGS ");
	wattroff(e->preview_c, COLOR_PAIR(4));
	wprintw(e->preview_c, "= %s", e->s_cflags);

	if (strlen(e->s_objdir))
	{
		wattron(e->preview_c, COLOR_PAIR(4));
		wprintw(e->preview_c, "\n\nOBJ_DIR ");
		wattroff(e->preview_c, COLOR_PAIR(4));
		wprintw(e->preview_c, "= %s", e->s_objdir);
	}
*/	
	wrefresh(e->preview);
	wrefresh(e->preview_c);
}

