/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   win_seldir.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 11:54:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 11:49:39 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	seldir_win(t_env *e)
{
	int				cols;
	int				lines;
	struct dirent	**dir;
	int				i;
	int				j;

	cols = v_in(COLS - 30, 30, 70);
	lines = v_in(LINES - 30, 10, 20);
	e->seldir = newwin(lines, cols,
			(LINES - lines) / 2, (COLS - cols) / 2);
	e->seldir_c = newwin(lines - 2, cols - 2,
			(LINES - lines) / 2 + 1, (COLS - cols) / 2 + 1);
	refresh();
	wattron(e->seldir, COLOR_PAIR(2));
	box(e->seldir, 0, 0);
	wattroff(e->seldir, COLOR_PAIR(2));
	mvwprintw(e->seldir, 0, 2, " SELECT FOLDER ");
	wrefresh(e->seldir);
	wattron(e->seldir_c, COLOR_PAIR(5) | A_BOLD);
	mvwprintw(e->seldir_c, 0, 2, "%s", e->dir);
	wattroff(e->seldir_c, COLOR_PAIR(5) | A_BOLD);
	dir = get_files(e->dir);
	i = -1;
	while (dir[++i])
	{
		if (i == e->list_sel)
			wattron(e->seldir_c, A_BOLD | A_REVERSE | COLOR_PAIR(sel_color(dir[i], e, 2, 6)));
		else
			wattron(e->seldir_c, COLOR_PAIR(sel_color(dir[i], e, 2, 3)));
		mvwprintw(e->seldir_c, i + 1, 1, "%s", dir[i]->d_name);
		j = strlen(dir[i]->d_name);
		while (++j < cols - 3)
			wprintw(e->seldir_c, " ");
		if (i == e->list_sel)
			wattroff(e->seldir_c, A_BOLD | A_REVERSE | COLOR_PAIR(sel_color(dir[i], e, 2, 6)));
		else
			wattron(e->seldir_c, COLOR_PAIR(sel_color(dir[i], e, 2, 3)));
		wprintw(e->seldir_c, "\n");
	}
	free(dir);
	wrefresh(e->seldir_c);
}
