/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper_files.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 14:02:53 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:51:58 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	fprint_var(t_win *win, FILE *file)
{
	int		nb;
	t_file	*tmp;
	int		len;

	fprintf(file, "\n\n%s ", win->name);
	if (win->files)
	{
		nb = count_selected_files(win);
		tmp = win->files;
		fprintf(file, "=");
		len = strlen(win->name) + 3;
		while (tmp)
		{
			if (tmp->selected)
			{
				nb--;
				if (len + strlen(tmp->name) + 3 > 80)
				{
					fprintf(file, " \\\n\t%s", tmp->name);
					len = strlen(tmp->name) + 4;
				}
				else
				{
					fprintf(file, " %s", tmp->name);
					len += strlen(tmp->name) + 1;
				}
			}
			tmp = tmp->next;
		}
		
	}
	else
		fprintf(file, "= %s", win->value);
}

void	fprint_phony(t_win *win, FILE *file)
{
	t_req	*tmp;

	fprintf(file, "\n\n%s : ", win->name);
	tmp = win->req;
	while (tmp)
	{
		fprintf(file, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
}

void	fprint_rule(t_win *win, FILE *file)
{
	t_req	*tmp;
	char	**lines;
	int		i;

	fprintf(file, "\n\n%s : ", win->name);
	tmp = win->req;
	while (tmp)
	{
		if (tmp->win->type == VAR)
			fprintf(file, "$(%s) ", tmp->win->name);
		else
			fprintf(file, "%s ", tmp->win->name);
		tmp = tmp->next;
	}
	lines = ft_split(win->value, '\n');
	if (!lines)
		return ;
	i = -1;
	while (lines[++i])
	{
		fprintf(file, "\n\t%s ", lines[i]);
		free(lines[i]);
	}
	free(lines);
}

void	fprint_object(t_win *win, FILE *file)
{
	t_req	*tmp;
	char	**lines;
	int		i;

	fprintf(file, "\n\n$(%s)%%.o : ", win->o_left->name);
	tmp = win->req;
	lines = ft_split(win->value, '\n');
	if (!lines)
		return ;
	i = -1;
	while (lines[++i])
	{
		fprintf(file, "\n\t%s ", lines[i]);
		free(lines[i]);
	}
	free(lines);
}

void	file_print(t_win *win, FILE *file)
{
	int	clr1;
	int	clr2;

	clr1 = 9;
	clr2 = 9;
	if (win->type == VAR)
		fprint_var(win, file);
	else if (win->type == PHONY)
		fprint_phony(win, file);
	else if (win->type == RULE)
		fprint_rule(win, file);
	else if (win->type == OBJECT)
		fprint_object(win, file);
}

void	generate_file(t_env *e, FILE *file)
{
	t_win	*win;
	
	fprintf(file, "%s", header());
	win = *e->windows;
	while (win)
	{
		file_print(win, file);
		win = win->next;
	}
}

void	save_makefile(t_env *e)
{
	FILE	*file;
	(void)e;
	file = fopen("Makefile_.mk", "w");
	generate_file(e, file);	
	fclose(file);
	rename("Makefile_.mk", "Makefile_");
}

void	close_input(t_env *e)
{
	e->mode = NORMAL;
	delwin(e->input);
	free(e->input_str);
	e->win_sel = e->prev_win_sel;
	e->input_str = strdup("");
	e->prev_key = -1;
	clear();
	add_wins(e);
}

void	save_input(t_env *e)
{
	e->mode = NORMAL;
	delwin(e->input);
	if (e->add_type == VAR)
		add_win_after(e, new_t_win(str_upper(strdup(e->input_str)),
				strdup(""), VAR, 1));
	else
		add_win_after(e, new_t_win(strdup(e->input_str),
				strdup(""), RULE, 1));
	e->win_sel = e->prev_win_sel + 1;
	free(e->input_str);
	e->input_str = strdup("");
	e->prev_key = -1;
	clear();
	add_wins(e);
}

void	close_selphony(t_env *e)
{
	delwin(e->selphony);
	e->win_sel = e->prev_win_sel;
	clear();
	e->mode = NORMAL;
	e->list_sel = 0;
	add_wins(e);
}

void	close_selreq(t_env *e)
{
	delwin(e->selreq);
	e->win_sel = e->prev_win_sel;
	clear();
	e->mode = NORMAL;
	e->list_sel = 0;
	add_wins(e);
}

void	close_selfiles(t_env *e)
{
	delwin(e->selfiles);
	e->win_sel = e->prev_win_sel;
	clear();
	e->mode = NORMAL;
	e->list_sel = 0;
	add_wins(e);
}

void	close_seldir(t_env *e)
{
	delwin(e->seldir);
	e->win_sel = e->prev_win_sel;
	free(e->dir);
	e->dir = malloc(sizeof(char) * 3);
	if (!e->dir)
		perr("MALLOC ERROR");
	e->dir = strdup("./");
	clear();
	e->mode = NORMAL;
	e->list_sel = 0;
	add_wins(e);
}

int	count_files(t_win *win)
{
	int		i;
	t_file	*tmp;

	i = 0;
	if (!win)
		return (0);
	tmp = win->files;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

void	save_files(t_env *e)
{
	t_win			*win;
	struct dirent	**files;
	int				i;

	delwin(e->seldir);
	e->win_sel = e->prev_win_sel;
	win = get_win_sel(e);	
	free_files(win);
	files = get_files(e->dir);
	i = -1;
	while (files[++i])
	{
		if (!is_dir(files[i], e)
				&& files[i]->d_name[strlen(files[i]->d_name) - 1] == 'c')
			add_file(win, new_t_file(strdup(files[i]->d_name)));
	}
	free(e->dir);
	e->dir = malloc(sizeof(char) * 3);
	if (!e->dir)
		perr("MALLOC ERROR");
	e->dir = strdup("./");
	clear();
	e->mode = SEARCHF2;
	e->win_sel = -1;
	add_wins(e);
	selfiles_win(e);
}

void	save_dir(t_env *e)
{
	t_win	*win;

	delwin(e->seldir);
	e->win_sel = e->prev_win_sel;
	win = get_win_sel(e);	
	free(win->value);
	win->value = malloc(sizeof(char) * (strlen(e->dir) + 1));
	if (!win->value)
		perr("MALLOC ERROR");
	win->value = strdup(e->dir);
	free(e->dir);
	e->dir = malloc(sizeof(char) * 3);
	if (!e->dir)
		perr("MALLOC ERROR");
	e->dir = strdup("./");
	clear();
	e->mode = NORMAL;
	add_wins(e);
}

void	get_prevdir(t_env *e)
{
	e->dir = prev_dir(e->dir);
	e->list_sel = 0;
	seldir_win(e);
}

void	get_subdir(t_env *e)
{
	struct dirent	**files;
	char			*dup;

	files = get_files(e->dir);
	if (is_dir(files[e->list_sel], e))
	{
		dup = malloc(sizeof(char) * (strlen(e->dir) + 1));
		if (!dup)
			perr("MALLOC ERROR");
		dup = strndup(e->dir, strlen(e->dir));
		dup[strlen(e->dir)] = 0;
		free(e->dir);
		e->dir = malloc(sizeof(char) + (strlen(dup) + strlen(files[e->list_sel]->d_name) + 2));
		if (!e->dir)
			perr("MALLOC ERROR");
		strncpy(e->dir, dup, strlen(dup));
		e->dir[strlen(dup)] = 0;
		free(dup);
		strncat(e->dir, files[e->list_sel]->d_name, strlen(files[e->list_sel]->d_name));
		e->dir[strlen(e->dir) + 1] = 0;
		e->dir[strlen(e->dir)] = '/';
		e->list_sel = 0;
		seldir_win(e);
	}
	free(files);
}

int	is_dir(struct dirent *file, t_env *e)
{
	if (file->d_type == 4)
	{
		if (file->d_name[0] != '.')
			return (1);
		else if (strlen(file->d_name) == 1)
			return (1);
		else if (strlen(file->d_name) == 2 && file->d_name[1] == '.'
				&& strlen(e->dir) != 2)
			return (1);
	}
	return (0);
}

int	sel_color(struct dirent *file, t_env *e, int a, int b)
{
	if (is_dir(file, e))
		return (a);
	return (b);
}

int is_only_subdir(char *dir)
{
	int	i;

	i = 0;
	while (dir[i])
	{
		if (dir[i] == '.' && dir[i + 1] && dir[i + 1] == '.'
				&& dir[i + 2] && dir[i + 2] == '/')
			i += 3;
		else
			return (0);
	}
	return (1);
}

char	*prev_dir(char *dir)
{
	char	*tmp;
	char	*pos;

	tmp = malloc(sizeof(char) * (strlen(dir) + 1));
	if (!tmp)
		perr("MALLOC ERROR");
	tmp = strdup(dir);
	free(dir);
	if (is_only_subdir(tmp))
	{
		dir = malloc(sizeof(char) * (strlen(tmp) + 4));
		strncpy(dir, tmp, strlen(tmp));
		strncat(dir, "../", 3);
		free(tmp);
		return (dir);
	}
	tmp[strlen(tmp) - 1] = 0;
	pos = strrchr(tmp, '/');
	if (!pos)
	{
		dir = malloc(sizeof(char) * 4);
		if (!dir)
			return (NULL);
		dir = strdup("../");
		free(tmp);
		return (dir);
	}
	*++pos = 0;
	dir = malloc(sizeof(char) * (strlen(tmp) + 1));
	strncpy(dir, tmp, strlen(tmp));
	free(tmp);
	return (dir);
}

int	get_nb_files(char *dir)
{
	DIR 			*dp;
	struct dirent	*ep;
	int				nb;

	nb = 0;
	dp = opendir(dir);
	if (dp != NULL)
	{
		while ((ep = readdir(dp)))
			nb++;
		closedir(dp);
	}
	return (nb);
}

struct dirent	**get_files(char *dir)
{
	DIR 			*dp;
	struct dirent	*ep;
	int				nb;
	struct dirent	**files;

	nb = get_nb_files(dir);
	files = malloc(sizeof(struct dirent *) * (nb + 1));
	if (!files)
		perr("MALLOC ERROR");
	dp = opendir(dir);
	if (dp != NULL)
	{
		nb = 0;
		while ((ep = readdir(dp)))
		{
			files[nb] = ep;
			nb++;
		}
		files[nb] = NULL;
	}
	return (files);
}
