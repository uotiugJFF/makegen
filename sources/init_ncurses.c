/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_ncurses.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:43:43 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:20:46 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/makegen.h"

void	init_clr()
{
	start_color();
	init_color(COLOR_BLACK, 150, 150, 150);
	init_color(COLOR_WHITE, 900, 800, 700);
	init_color(COLOR_GREEN, 500, 600, 0);
	init_color(COLOR_YELLOW, 1000, 650, 150);
	init_color(COLOR_CYAN, 400, 400, 400);
	init_color(COLOR_BLUE, 300, 600, 750);
	init_color(COLOR_RED, 750, 300, 300);
	init_pair(1, COLOR_GREEN, COLOR_WHITE);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	init_pair(3, COLOR_CYAN, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_GREEN, COLOR_BLACK);
	init_pair(6, COLOR_CYAN, COLOR_WHITE);
	init_pair(7, COLOR_YELLOW, COLOR_WHITE);
	init_pair(8, COLOR_RED, COLOR_BLACK);
	init_pair(9, COLOR_WHITE, COLOR_BLACK);
}

void	init_scr(t_env *e)
{
    initscr();
	e->win_sel = 0;
	e->mode = NORMAL;
	e->list_sel = 0;
	e->dir = strndup("./", 3);
	e->input_str = strndup("", 2);
	raw();
	noecho();
	keypad(stdscr, TRUE);
	init_clr();
	refresh();
	add_wins(e);
}

void	init_vars(t_env *e)
{
	t_win	*obj_dir;
	t_win	*src_dir;
	t_win	*o_c;
	
	add_win_back(e->windows, new_t_win(strdup("NAME"), strdup(""), VAR, 0));
	add_win_back(e->windows, new_t_win(strdup("CC"), strdup("cc"), VAR, 0));
	add_win_back(e->windows, new_t_win(strdup("CFLAGS"),
				strdup("-Wall -Wextra -Werror"), VAR, 1));
	add_win_back(e->windows, new_t_win(strdup("INCLUDES"), strdup(""), VAR, 0));
	src_dir = new_t_win(strdup("SRC_DIR"), strdup(""), VAR, 1);
	add_win_back(e->windows, src_dir);
	obj_dir = new_t_win(strdup("OBJ_DIR"), strdup(""), VAR, 1);
	add_win_back(e->windows, obj_dir);
	add_win_back(e->windows, new_t_win(strdup("SRC"), strdup(""), VAR, 1));
	add_win_back(e->windows, new_t_win(strdup("SRC_FILES"),
				strdup("$(addprefix $(SRC_DIR), $(SRC))"), VAR, 1));
	add_win_back(e->windows, new_t_win(strdup("OBJ_FILES"),
				strdup("$(addprefix $(OBJ_DIR), $(OBJ))"), VAR, 1));
	o_c = new_t_win(strdup(""), strdup("mkdir -p $(OBJ_DIR)\n$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@"), OBJECT, 1);
	add_win_back(e->windows, o_c);
	o_c->o_left = obj_dir;
	o_c->o_right = src_dir;
	add_win_back(e->windows, new_t_win(strdup(".PHONY"), strdup(""), PHONY, 1));
	add_win_back(e->windows, new_t_win(strdup("all"), strdup(""), RULE, 1));
	add_win_back(e->windows, new_t_win(strdup("$(NAME)"), strdup("$(CC) $(CFLAGS) $(OBJ_FILES)"), RULE, 0));
	add_req_back("all", "NAME", e);
	add_req_back("$(NAME)", "OBJ_FILES", e);
	add_win_back(e->windows, new_t_win(strdup("clean"), strdup("$(RM) -rf $(OBJ_DIR)"), RULE, 1));
	add_win_back(e->windows, new_t_win(strdup("fclean"), strdup("$(RM) $(NAME)"), RULE, 1));
	add_req_back("fclean", "clean", e);
	add_win_back(e->windows, new_t_win(strdup("re"), strdup(""), RULE, 1));
	add_req_back("re", "fclean", e);
	add_req_back("re", "all", e);
	add_win_back(e->windows, new_t_win(strdup("norme"),
				strdup("@norminette $(SRC_FILES) | grep -v Norme -B1 || true\n@norminette $(INCLUDES) -R CheckDefine | grep -v Norme -B1 || true"), RULE, 1));
	add_req_back(".PHONY", "all", e);
	add_req_back(".PHONY", "clean", e);
	add_req_back(".PHONY", "fclean", e);
	add_req_back(".PHONY", "re", e);
	add_req_back(".PHONY", "norme", e);
}

