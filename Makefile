# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/11/28 10:23:24 by gbrunet           #+#    #+#              #
#    Updated: 2023/12/01 11:51:49 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = makegen

CC = cc

CFLAGS = -Wall -Wextra -Werror

NCURSES = -lcurses

LIBFT = ./libft/

OBJ_DIR = objects/

SRC_DIR = sources/

SRC = main header \
	  win_preview win_status win_name win_seldir win_selreq win_selphony \
	  win_input win_selfiles \
	  init_ncurses \
	  helper_wins helper_keys helper_strings helper_errors helper_files \
	  helper_numbers helper_sel helper_required helper_left_wins helper_phony \
	  helper_t_files

SRC_FILES = $(addprefix $(SRC_DIR), $(addsuffix .c, $(SRC)))

OBJ_FILES = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC)))

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY : all clean fclean re norme

all : $(NAME)

lib :
	make -C $(LIBFT)

$(NAME) : lib $(OBJ_FILES)
	$(CC) $(CFLAGS) $(NCURSES) $(OBJ_FILES) -o $(NAME) -lft -L ./libft

clean :
	$(RM) -rf $(OBJ_DIR)

fclean : clean
	$(RM) $(NAME)

re : fclean all

norme :
	@norminette $(SRC_FILES) | grep -v Norme -B1 || true
	@norminette makegen.h -R CheckDefine | grep -v Norme -B1 || true
