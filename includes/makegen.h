/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makegen.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/28 10:21:33 by gbrunet           #+#    #+#             */
/*   Updated: 2023/12/01 18:26:20 by gbrunet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAKEGEN_H
# define MAKEGEN_H

# include <ncurses.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
# include <ctype.h>
# include <stdio.h>
# include <sys/types.h>
# include <dirent.h>
# include <sys/stat.h>
# include <unistd.h>
# include "../libft/libft.h"

enum mode {
	NORMAL,
	INSERT,
	SEARCH,
	SEARCHF,
	SEARCHF2,
	SELECT,
	SPHONY,
	INPUT,
	OBJECTS,
};

enum w_type {
	RULE,
	VAR,
	PHONY,
	OBJECT,
};

typedef struct s_env {
	int				win_sel;
	int				prev_win_sel;
	WINDOW			*preview;
	WINDOW			*preview_c;
	WINDOW			*status;
	WINDOW			*seldir;
	WINDOW			*seldir_c;
	WINDOW			*selfiles;
	WINDOW			*selfiles_c;
	WINDOW			*selobject;
	WINDOW			*selobject_c;
	WINDOW			*selreq;
	WINDOW			*selreq_c;
	WINDOW			*selphony;
	WINDOW			*selphony_c;
	WINDOW			*input;
	WINDOW			*input_c;
	int				col;
	char			*input_str;
	char			*dir;
	int				list_sel;
	struct s_win	**windows;
	int				left_win_pos;
	enum mode		mode;
	int				prev_key;
	enum w_type		add_type;
	int				cursor_pos;
	struct s_win	*o_left;
	struct s_win	*o_right;
}	t_env;

typedef	struct s_file {
	char			*name;
	int				selected;
	struct s_file	*next;
}	t_file;

typedef	struct s_req {
	struct s_win	*win;
	struct s_req	*prev;
	struct s_req	*next;
}	t_req;

typedef struct s_win {
	WINDOW			*win;
	WINDOW			*sub;
	char			*name;
	char			*value;
	char			*object;
	int				pos;
	struct s_win	*prev;
	struct s_win	*next;
	enum w_type		type;
	struct s_req	*req;
	struct s_file	*files;
	struct s_win	*o_left;
	struct s_win	*o_right;
	int				can_del;
}	t_win;

int				is_tab(int key);
int				is_enter(int key);
int				max(int a, int b);
int				min(int a, int b);
int				get_nb_files(char *dir);
int				v_in(int v, int a, int b);
int				is_only_subdir(char *dir);
int				is_dir(struct dirent *file, t_env *e);
int				sel_color(struct dirent *file, t_env *e, int a, int b);

char			*header();
char			*prev_dir(char *dir);
char			*update_str(char *str, int key, t_env *e);

void			init_clr();
void			cc_win(t_env *e);
void			perr(char *err);
void			init_scr(t_env *e);
void			add_wins(t_env *e);
void			name_win(t_env *e);
void			init_vars(t_env *e);
void			cflags_win(t_env *e);
void			objdir_win(t_env *e);
void			seldir_win(t_env *e);
void			status_win(t_env *e);
void			get_subdir(t_env *e);
void			save_dir(t_env *e);
void			get_prevdir(t_env *e);
void			preview_win(t_env *e);
void			do_backspace(t_env *e);
void			resize_update(t_env *e);
void			focus_sel_win(t_env *e);
void			remove_last_c(char *str, t_env *e);
void			get_ch(t_env *e, int key);

enum win		next_win(enum win current_win);
enum win 		prev_win(enum win current_win);

struct dirent	**get_files(char *dir);

t_win			*get_win_sel(t_env *e);
void			del_t_win(t_env *e);
void			set_prev_sel(t_env *e);
t_win			*get_win_named(char *name, t_env *e);
void			selreq_win(t_env *e);
void			close_seldir(t_env *e);
void			close_selreq(t_env *e);
t_win			*get_prev_win_sel(t_env *e);
int				is_required(t_win *win, char *name);
void			add_req_back(char *win_str, char *req_win_str, t_env *e);
void			del_req(t_win *win, char *win_del);
void			set_prev_sel(t_env *e);
void			set_next_sel(t_env *e);
void			normal_mode(t_env *e, int key);
void			insert_mode(t_env *e, int key);
void			search_mode(t_env *e, int key);
void			select_mode(t_env *e, int key);
int				count_req(t_env *e);
void			toggle_required(t_env *e);
int				count_win(t_env *e);
int				get_lb_nb(char *str);
t_req			*new_t_req(t_win *win);
void			add_win_back(t_win **list, t_win *win);
t_win			*new_t_win(char *name, char *value, enum w_type type, int can_del);
void			selphony_win(t_env *e);
void			phony_mode(t_env *e, int key);
int				count_phony(t_env *e);
void			toggle_phony(t_env *e);
void			close_selphony(t_env *e);
void			add_var(t_env *e);
void			input_win(t_env *e);
void			close_input(t_env *e);
void			do_backspace_input(t_env *e);
void			get_ch_input(t_env *e, int key);
void			input_mode(t_env *e, int key);
void			save_input(t_env *e);
char			*str_upper(char *str);
void			add_win_after(t_env *e, t_win *win);
void			add_rule(t_env *e);
void			add_phony(t_env *e);
void			save_files(t_env *e);
void			add_file(t_win *win, t_file *file);
t_file			*new_t_file(char *name);
int				count_files(t_win *win);
void			selfiles_win(t_env *e);
void			toggle_file(t_env *e);
void			close_selfiles(t_env *e);
int				count_selected_files(t_win *win);
void			free_files(t_win *win);
int				all_files_selected(t_win *win);
void			cursor_left(t_env *e);
void			cursor_right(t_env *e);
void			add_object(t_env *e);
void			object_win(t_env *e);
int				count_object(t_env *e);
void			set_object(t_env *e);
void			close_object(t_env *e);
void			object_mode(t_env *e, int key);
void			save_object(t_env *e);
void			save_makefile(t_env *e);

#endif
